# my_vimrc

#### 项目介绍
windows上 gvim 配置  文件里写的都很清楚，直接看就好了

#### hotkey
修改默认leader键为  ``` ``` 空格键
<br>
#### fzf文件搜索
```<leader>+f``` 打开文件搜索
<br>
#### nerdTree 打开
```<leader>+t```
<br>
#### tagbar 显示文件函数
按下 F8 
<br>
#### buffer 
新建buffer         ```<leader>+p```
<br>
关闭当前buffer      ```<leader>+w```
<br>
上一个buffer       ```<leader>+b```
<br>
下一个buffer       ```<leader>+n```
<br>
查看buffer  ```<leader>+l```
<br>
索引快速跳转 
```<leader>+1```
```<leader>+2```
....
<br>
#### nerdTree
在NERDTree中选中目录，然后按ma，就可以新建文件或者目录
<br>
#### 快速注释(nerdComment)
```<leader>cc```  注释
<br>
```<leader>cu```  解除注释
<br>
#### 多光标
```ctrl+n ```     向下多选
<br>
```esc```         取消选择
<br>
#### emmet
```,,```  触发按键改成 ',' 这样的话双击很方便
<br>
#### 在当前文件里搜索光标所在的world
```<leader>ff```  之后会出现列表，输入对应的数字可以跳转到对应world所在行
<br>
#### 去掉搜索高亮
``` <leader>/ ```
