" encoding 
set encoding=utf-8
set fileencodings=utf-8,gb18030,utf-16,big5
source $VIMRUNTIME/delmenu.vim
source $VIMRUNTIME/menu.vim
language messages zh_CN.utf-8

"---------------------general-----------------
set textwidth=70
set formatoptions+=mM
" auto indent 
set autoindent              
set smartindent            

set backspace=indent,eol,start

filetype on         
filetype plugin on 

set vb t_vb=
au GuiEnter * set t_vb=

autocmd GUIEnter * simalt ~x

" Time to wait for a command
set timeoutlen=800

" Remap ; to : in visual mode
nnoremap ; :

" <leader> mapping to space key
let mapleader=" "

set winaltkeys=no

set nocompatible

set nu
augroup relative_numbser
		autocmd!
		autocmd InsertEnter * :set norelativenumber
		autocmd InsertLeave * :set relativenumber
augroup END

set tabstop=2

" set guifont=Fira_Code:h15

set guifont=Office_Code_Pro_D_Medium:h13:cANSI

set clipboard+=unnamed 
" runtimepath
set rtp+=$HOME\vimfiles
" for fzf
set rtp+=$HOME\vimfiles\bundle\fzf

set ignorecase " Case insensitive search
set smartcase " Case sensitive when uc present
set hlsearch " Highlight search terms
set incsearch " Find as you type search
set gdefault " turn on g flag

set showmatch

set cursorline
highlight Cursor guifg=white guibg=black
" set cursorcolumn
highlight CursorLine cterm=none ctermfg=white ctermbg=darkred guibg=darkred guifg=white
 " highlight CursorColumn cterm=none ctermbg=236

"-----------theme config------------------
syntax enable
syntax on

colorscheme dracula
" set bg=dark                    
" colorscheme gruvbox           
" colorscheme escuro 
" let base16colorspace=256
" colorscheme base16-oceanicnext
" colorscheme molokai

set guioptions=             

"-------------Status lightline--------------------

set t_Co=256
set laststatus=2
set lazyredraw

" let g:airline_theme='luna' 
" let g:airline_theme='onedark' 
"let g:airline_theme='molokai' 
let g:airline_theme="powerlineish" 
" let g:airline_theme="dracula"
" let g:dracula_italic = 0
" let g:airline_theme="gruvbox" 
let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline#extensions#tabline#left_alt_sep =  ''   
let g:airline#extensions#whitespace#enabled = 0

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''




"----------------------hotkey config----------------------

" Deleted search highlight
nmap <leader>/ :noh<CR>

" fast save and exit
nmap fw     :w<CR>
nmap fq     :q<CR>
nmap fwq    :wq<CR>

" move currnet line
nnoremap [e  :<c-u>execute 'move -1-'. v:count1<cr>
nnoremap ]e  :<c-u>execute 'move +'. v:count1<cr>

" fast add black line
nnoremap [<space>  :<c-u>put! =repeat(nr2char(10), v:count1)<cr>'[
nnoremap ]<space>  :<c-u>put =repeat(nr2char(10), v:count1)<cr>

" Navigation between windows
nnoremap <C-J> <C-W>j
nnoremap <C-K> <C-W>k
nnoremap <C-H> <C-W>h
nnoremap <C-L> <C-W>l

" Adjust viewports to the same size
map <Leader>= <C-w>=

" Map <Leader>ff to display all lines with keyword under cursor
" and ask which one to jump to
nnoremap <SID>FindTextUnderCursor [I:let nr = input("Which one to jump to: ")<Bar>exe "normal " . nr ."[\t"<CR>
nmap <Leader>ff <SID>FindTextUnderCursor



"----------vim-plug config---------------
" plugin-list
call plug#begin('$HOME\vimfiles\bundle')
" for fzf
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'mileszs/ack.vim'

" theme
Plug 'morhetz/gruvbox'
Plug 'rakr/vim-one'
Plug 'joshdick/onedark.vim'
Plug 'chriskempson/base16-vim'
Plug 'dracula/vim', { 'as': 'dracula' }

" prettier 
Plug 'prettier/vim-prettier' , {
  \ 'do': 'npm install',
  \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue', 'yaml', 'html', 'vue'] }

" ultisnips
Plug 'sirver/ultisnips'
Plug 'honza/vim-snippets'

" nerdTree
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'

" vue plugin 
Plug 'posva/vim-vue'

" css plugin 
Plug 'juleswang/css.vim'

" statusline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" tagbar plugin 
Plug 'majutsushi/tagbar'

" keyword fast move
Plug 'easymotion/vim-easymotion'

" fast comment plugin 
Plug 'ddollar/nerdcommenter'

" multiple cursors 
Plug 'terryma/vim-multiple-cursors'

" startup page
Plug 'mhinz/vim-startify'

" auto completion bracket...
Plug 'raimondi/delimitmate'

" emmet
Plug 'mattn/emmet-vim'

" auto completion path or functions or api...
Plug 'shougo/neocomplete.vim'

call plug#end()
"-------------------buffers------------------

" Open a new instance tab
nnoremap <Leader>p :tabnew +Startify<CR>

" close currnet tab
nnoremap <leader>w :bdelete<CR>

" switch buffers
nnoremap <Leader>b :bp<CR>
nnoremap <Leader>n :bn<CR>

" check buffers list	
nnoremap <Leader>l :ls<CR>

" number move buffers 
nnoremap <Leader>1 :1b<CR>
nnoremap <Leader>2 :2b<CR>
nnoremap <Leader>3 :3b<CR>
nnoremap <Leader>4 :4b<CR>
nnoremap <Leader>5 :5b<CR>
nnoremap <Leader>6 :6b<CR>
nnoremap <Leader>7 :7b<CR>
nnoremap <Leader>8 :8b<CR>
nnoremap <Leader>9 :9b<CR>
nnoremap <Leader>0 :10b<CR>

" ---------------plug config----------------

" emmet
"enable all functions, which is equal to
let g:user_emmet_leader_key=','
" let g:user_emmet_mode='inv'  

nmap <silent> <leader>f :Files<CR>

command! -bang -nargs=* Ag
						\ call fzf#vim#ag(<q-args>,
						\                 <bang>0 ? fzf#vim#with_preview('up:60%')
						\                         : fzf#vim#with_preview('right:50%:hidden', '?'),
						\                 <bang>0)
nnoremap <silent> <Leader>a :Ag<CR>

"nerdTree config
map <F1> :NERDTreeToggle<CR>
nmap <leader>m :NERDTreeFind<CR>
let NERDTreeShowLineNumbers=1
let NERDTreeAutoCenter=1
let NERDTreeShowHidden=1
let NERDTreeWinSize=26
let g:nerdtree_tabs_open_on_console_startup=1
let NERDTreeIgnore=['\.pyc','\~$','\.swp']
" nerdTree git
let g:NERDTreeIndicatorMapCustom = {
    \ "Modified"  : "�7�5",
    \ "Staged"    : "�7�4",
    \ "Untracked" : "�7�3",
    \ "Renamed"   : "�7�4",
    \ "Unmerged"  : "�T",
    \ "Deleted"   : "�7�0",
    \ "Dirty"     : "�7�1",
    \ "Clean"     : "�7�8�1�4",
    \ "Unknown"   : "?"
    \ }



" tagbar config
map <F2> :TagbarToggle<CR>
let g:tagbar_autoclose = 1


" nerdComment config
let g:NERDSpaceDelims=1

" 
" Multip Cursor
" Default key mapping
let g:multi_cursor_next_key='<C-n>'
let g:multi_cursor_prev_key='<C-p>'
let g:multi_cursor_skip_key='<C-x>'
let g:multi_cursor_quit_key='<Esc>'

" Called once right before you start selecting multiple cursors
function! Multiple_cursors_before()
  if exists(':NeoCompleteLock')==2
	exe 'NeoCompleteLock'
  endif
endfunction

" Called once only when the multiple selection is canceled (default <Esc>)
function! Multiple_cursors_after()
  if exists(':NeoCompleteUnlock')==2
	exe 'NeoCompleteUnlock'
  endif
endfunction



"------------------neocomplete config ----------------------"	
" Use neocomplete.
let g:neocomplete#enable_at_startup = 1
" Use smartcase.
let g:neocomplete#enable_smart_case = 1
nnoremap <silent><F4> :NeoCompleteToggle<CR>
" Define dictionary.
let g:neocomplete#sources#dictionary#dictionaries = {
    \ 'default' : '',
    \ 'vimshell' : $HOME.'/.vimshell_hist',
    \ 'scheme' : $HOME.'/.gosh_completions'
        \ }

" Define keyword.
if !exists('g:neocomplete#keyword_patterns')
    let g:neocomplete#keyword_patterns = {}
endif
let g:neocomplete#keyword_patterns['default'] = '\h\w*'

" <TAB>: completion.
inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<TAB>"

" prettier config
nmap <F3> <Plug>(Prettier)

" lose focus auto execute prettier
let g:prettier#autoformat = 0
let g:prettier#config#trailing_comma = 'none'
autocmd BufWritePre *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.yaml,*.html PrettierAsync

" ultisnip config
" let g:UltiSnipsExpandTrigger="<tab>"
" 使用 tab 切换下一个触发点，shit+tab 上一个触发点
let g:UltiSnipsJumpForwardTrigger="<C-f>"
let g:UltiSnipsJumpBackwardTrigger="<C-b>"

